.\" Adduser and this manpage are copyright 1995 by Ted Hajek
.\"
.\" This is free software; see the GNU General Public Lisence version 2
.\" or later for copying conditions.  There is NO warranty.
.TH ADDUSER.CONF 5 "" "Debian GNU/Linux"
.SH NAME
/etc/adduser.conf \- configuration file for 
.BR adduser (8) 
and 
.BR addgroup (8).
.SH DESCRIPTION
The file \fI/etc/adduser.conf\fP contains defaults for the programs
.BR adduser (8), 
.BR addgroup (8),
.BR deluser (8)
and 
.BR delgroup (8).
Each line holds a single value pair in the form \fIoption\fP = \fIvalue\fP.
Double or single quotes are allowed around the value, as is whitespace
around the equals sign.
Comment lines must have a hash sign (#) in the first column.

The valid configuration options are:
.TP
.B DSHELL
The login shell to be used for all new users.
Defaults to \fI/bin/bash\fP.
.TP
.B DHOME
The directory in which new home directories should be created.
Defaults to \fI/home\fP.
.TP
.B GROUPHOMES
If this is set to \fIyes\fP, the home directories will be created as
\fI/home/groupname/user\fP.
Defaults to \fIno\fP.
.TP
.B LETTERHOMES
If this is set to \fIyes\fP, then the home directories created will
have an extra directory inserted which is the first letter
of the loginname.
For example: \fI/home/u/user\fP.
Defaults to \fIno\fP.
.TP
.B SKEL
The directory from which skeletal user configuration files should be
copied.  Defaults to \fI/etc/skel\fP.
.TP
.BR FIRST_SYSTEM_UID " and " LAST_SYSTEM_UID
specify an inclusive range of UIDs from which system UIDs can be
dynamically allocated.
Default to \fI100\fP - \fI999\fP.
Please note that system software, such as the users allocated by the
base-passwd package, may assume that UIDs less than 100 are unallocated.
.TP
.BR FIRST_UID " and " LAST_UID
specify an inclusive range of UIDs from which normal user's UIDs can
be dynamically allocated.
Default to \fI1000\fP - \fI59999\fP.
.TP
.BR FIRST_SYSTEM_GID " and " LAST_SYSTEM_GID
specify an inclusive range of GIDs from which system GIDs can be
dynamically allocated.
Default to \fI100\fP - \fI999\fP.
.TP
.BR FIRST_GID " and " LAST_GID
specify an inclusive range of GIDs from which normal group's GIDs can
be dynamically allocated.
Default to \fI1000\fP - \fI59999\fP.
.TP
.B USERGROUPS
If this is set to \fIyes\fP, then each created non-system user will
be given their own group to use.
The default is \fIyes\fP.
.TP
.B USERS_GID
.B USERS_GROUP
Defines the group name or GID of the group all newly-created non-system users
are placed into. If \fBUSERGROUPS\fP is \fIyes,\fP the group will be added as
a supplementary group; if \fBUSERGROUPS\fP is \fIno,\fP, it will be the
primary group. If you don't want all your users to be in one group, set
\fBUSERGROUPS\fP is \fIyes\fP, leave \fBUSERS_GROUP\fP empty and set
\fBUSERS_GID\fP to "-1".
The default value of USERS_GROUP is \fIusers\fP, which has GID 100 on
all Debian systems since it's defined statically by the \fIbase-passwd\fP
package.
.TP
.B DIR_MODE
If set to a valid value (e.g. 0755 or 755), directories created will have
the specified permissions mode. Otherwise 2700 is used as default.
(See SYS_DIR_MODE for system users.)  Note that there are potential
configurations (such as /~user web services, or in-home mail delivery)
which will require changes to the default.
.TP
\.B SYS_DIR_MODE
If set to a valid value (e.g. 0755 or 755), directories created for
system users will have the specified permissions mode.  Otherwise
0755 is used as default.  Note that changing the default permissions
for system users may cause some packages to behave unreliably, if
the program relies on the default setting.
.TP
.B SETGID_HOME
If this is set to \fIyes\fP, then home directories for users with
their own group (\fBUSERGROUPS\fP = yes) will have the setgid bit set.
This is the default setting for normal
user accounts.  If you set this to "no", you should also change the
value of DIR_MODE, as the default (2700) sets this bit regardless.  Note
that this feature is \fBdeprecated\fP and will be removed in a future
version of \fBadduser\fP.
Please use \fBDIR_MODE\fP instead.
.TP
.B QUOTAUSER
If set to a nonempty value, new users will have quotas copied from
that user.
The default is empty.
.TP
.B NAME_REGEX
User and group names are checked against this regular expression. If the name
doesn't match this regexp, user and group creation in adduser is refused unless
--allow-badname is set. With --allow-badname set, only weak checks are
performed. The default is the most conservative ^[a-z][-a-z0-9_]*$. See
\fBValid names\fP, below, for more information.
.TP
.B SYS_NAME_REGEX
System user and group names are checked against this regular expression. If
this variable is not set, it falls back to the default value.
If the name doesn't match this regexp, system
user and group creation in adduser is refused unless --allow-badname is
set. With --allow-badname set, only weak checks are performed. The default
is the most conservative ^[a-z_][-a-z0-9_]*$.  See
\fBValid names\fP, below, for more information.
.TP
.B SKEL_IGNORE_REGEX
Files in \fI/etc/skel/\fP are checked against this regex, and not
copied to the newly created home directory if they match.
This is by default set to the regular expression matching files left over
from unmerged config files (dpkg-(old|new|dist)).
.TP
.B ADD_EXTRA_GROUPS
Setting this to something other than 0 (the default) will cause
\fBadduser\fP to add newly created non-system users to the list of
groups defined by  \fBEXTRA_GROUPS\fP (below).
.TP
.B EXTRA_GROUPS
This is the space-separated list of groups that new non-system users
will be added to.
.SH NOTES
.TP
.B VALID NAMES
.TP
Historically, \fBadduser\fP and \fBaddgroup\fP enforced conformity
to IEEE Std 1003.1-2001, which allows only the following characters
to appear in group and user names: letters, digits, underscores,
periods, at signs (@) and dashes.
The name may not start with a dash or @.
The "$" sign is allowed at the end of usernames (to conform to samba).
.TP
The default settings for \fBNAME_REGEX\P and \fBSYS_NAME_REGEX\fP
allow usernames to contain lowercase letters and numbers, plus dash (-)
and underscore (_); the name must begin with a letter (or an underscore
for system users).
.TP
The least restrictive policy, available by using the \fB\-\-allow-all-names\fP
option, simply makes the same checks as \fBuseradd\fP: cannot start with a dash,
plus sign, or tilde; and cannot contain a colon, comma, slash, or whitespace.
.TP
This option can be used to create confusing or misleading names; use
it with caution.
.TP
Please note that regardless of the regular expressions used to evaluate
the username, it may be a maximum of 32 bytes; this may be less than 32
visual characters when using Unicode glyphs in the username.

.SH FILES
.I /etc/adduser.conf
.SH SEE ALSO
.BR deluser.conf (5),
.BR addgroup (8),
.BR adduser (8),
.BR delgroup (8),
.BR deluser (8)
