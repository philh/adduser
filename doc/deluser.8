.\" Copyright 1997, 1998, 1999 Guy Maor.
.\" Adduser and this manpage are copyright 1995 by Ted Hajek,
.\" With much borrowing from the original adduser copyright 1994 by
.\" Ian Murdock.
.\" 
.\" This is free software; see the GNU General Public License version
.\" 2 or later for copying conditions.  There is NO warranty.
.TH DELUSER 8 "" "Debian GNU/Linux"
.SH NAME
deluser, delgroup \- remove a user or group from the system
.SH SYNOPSIS
.SY deluser
.OP [options]
.OP \-\-no-preserve-root
.OP \-\-remove-home
.OP \-\-remove-all-files
.OP \-\-backup
.OP \-\-backup-to dir
.OP user
.SY deluser
.OP \-\-group
.OP [options]
.OP group
.SY delgroup
.OP [options]
.OP \-\-only-if-empty
.OP group
.SY deluser
.OP [options]
.OP user
.OP group
.YS
.SH DESCRIPTION
.PP
\fBdeluser\fP and \fBdelgroup\fP remove users and groups from the system
according to command line options and configuration information in
\fI/etc/deluser.conf\fP and \fI/etc/adduser.conf\fP.
They are friendlier front ends to the \fBuserdel\fP and \fBgroupdel\fP
programs, removing the home directory as option or even all files on the system
owned by the user to be removed, running a custom script, and other features.
\fBdeluser\fP and \fBdelgroup\fP can be run in one of three modes:
.SS "Remove a normal user"
If called with one non-option argument and without the \fB\-\-group\fP option,
\fBdeluser\fP will remove a normal user.

By default, \fBdeluser\fP will remove the user without removing the home
directory, the mail spool  or any other files on the system owned by the user.
Removing the home directory and mail spool can be achieved using the
\fB\-\-remove-home\fP option. 

The  \fB\-\-remove-all-files\fP option removes all files on the system
owned by the user.
Note that if you activate both options \fB\-\-remove-home\fP will have
no effect because all files including the home directory and mail
spool are already covered by the \fB\-\-remove-all-files\fP option.

If you want to backup all files before deleting them you can activate the
\fB\-\-backup\fP option which will create a file \fIusername.tar(.gz|.bz2)\fP
in the directory specified by the \fB\-\-backup-to\fP option
(defaulting to the current working directory).

  By default, the backup archive is compressed with gzip. To change this,
the \fB\-\-backup-suffix\fP option can be set to any suffix supported by
\fBtar --auto-compress\fP (e.g. .gz, .bz2, .xz).

The remove, suffix, and backup options can also be activated by
default in the configuration file \fIetc/deluser.conf\fP. See
.BR deluser.conf(5)
for details.

If you want to remove the root account (uid 0), then use the 
\fB\-\-no-preserve-root\fP parameter; this may prevent to remove the root
user by accident.

If the file \fI/usr/local/sbin/deluser.local\fP exists,
it will be executed after the user account has been removed
in order to do any local cleanup.
The arguments passed to \fBdeluser.local\fP are:
.br
.I "username uid gid home-directory"

.SS "Remove a group"
If \fBdeluser\fP is called with the \fB\-\-group\fP  option, or
\fBdelgroup\fP is called, a group will be removed.

Warning: The primary group of an existing user cannot be removed.

If the option \fB\-\-only-if-empty\fP is given, the group
won't be removed if it has any members left.

.SS "Remove a user from a specific group"
If called with two non-option arguments, \fBdeluser\fP
will remove a user from a specific group.
.SH OPTIONS
.TP
.BR "\-\-conf \fIfile",  "\-c \fIfile\fP" 
Use \fIfile\fP instead of the default files \fI/etc/deluser.conf\fP
and \fI/etc/adduser.conf\fP.
.TP
.B \-\-group
Remove a group. This is the default action if the program is invoked
as \fIdelgroup\fP.
.TP
.BR \-\-help ", "\-h
Display brief instructions.
.TP
.B \-\-quiet, \-q
Suppress progress messages.
.TP
.B \-\-debug
Be verbose, most useful if you want to nail down a problem.
.TP
.B \-\-system
Only delete if user/group is a system user/group. This avoids
accidentally deleting non-system users/groups. Additionally, if the
user does not exist, no error value is returned. Debian package maintainer
scripts may use this flag to remove system users or groups while ignoring the
case where the removal already occurred.
.TP
.B \-\-only-if-empty 
Only remove if no members are left.
.TP
.B \-\-backup
Backup all files contained in the userhome and the mailspool file
to a file named \fIusername.tar.bz2\fP or \fIusername.tar.gz\fP.
.TP
.BI "\-\-backup-to "dir
Place the backup files not in the current directory but in \fIdir\fP.
This implicitly sets \fB\-\-backup\fP also.
.TP
.B \-\-remove-home
Remove the home directory of the user and its mailspool.
If \fB\-\-backup\fP is specified, the files are deleted after
having performed the backup.
.TP
.B \-\-remove-all-files
Remove all files from the system owned by this user.
Note: \-\-remove-home does not have an effect any more.
If \fB\-\-backup\fP is specified, the files are deleted after
having performed the backup.
.TP
.B \-\-version
Display version and copyright information.
.SH "RETURN VALUE"
.TP
.B 0
Success: The action was successfully executed.
.TP
.B 1
The user to delete was not a system account.
No action was performed.
.TP
.B 2
There is no such user.
No action was performed.
.TP
.B 3
There is no such group.
No action was performed.
.TP
.B 4
Internal error.
No action was performed.
.TP
.B 5
The group to delete is not empty.
No action was performed.
.TP
.B 6
The user does not belong to the specified group.
No action was performed.
.TP
.B 7
You cannot remove a user from its primary group.
No action was performed.
.TP
.B 8
The required perl 'perl' is not installed.
This package is required to perform the requested actions.
No action was performed.
.TP
.B 9
For removing the root account the parameter \fB--no-preserve-root\fP
is required.
No action was performed.


.SH SECURITY
\fBdeluser\fP needs root privileges and offers, via the \fB\-\-conf\fP
command line option to use a different configuration file. Do not use
\fBsudo\fP or similar tools to give partial privileges to \fBdeluser\fP
with restricted command line parameters. This is easy to circumvent and might
allow users to create arbitrary accounts. If you want this, consider writing
your own wrapper script and giving privileges to execute that script.

.SH FILES
.IR /etc/deluser.conf
Default configuration file for \fBdeluser\fP and \fBdelgroup\fP
.TP
.IR /usr/local/sbin/deluser.local
Optional custom add-ons.

.SH "SEE ALSO"
.BR adduser (8),
.BR deluser.conf (5),
.BR groupdel (8),
.BR userdel (8)

.SH COPYRIGHT
Copyright (C) 2000 Roland Bauerschmidt. Modifications (C) 2004
Marc Haber and Joerg Hoh.
This manpage and the \fBdeluser\fP program are based on \fBadduser\fP which is:
.br
Copyright (C) 1997, 1998, 1999 Guy Maor.
.br
Copyright (C) 1995 Ted Hajek, with a great deal borrowed from the original
Debian \fBadduser\fP
.br
Copyright (C) 1994 Ian Murdock.
\fBdeluser\fP is free software; see the GNU General Public Licence
version 2 or later for copying conditions.  There is \fIno\fP warranty.
